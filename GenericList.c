//
// Created by barrow099 on 10/25/18.
//

#include <stdlib.h>
#include <stdbool.h>
#include "GenericList.h"


GenericItem* private_get_item(GenericItem* list, int index);
GenericItem* private_create_item(void* value);


GenericItem *gl_create_list() {
    GenericItem *head = malloc(sizeof(GenericItem));
    head->value = NULL;
    return head;
}

void *gl_get_item(GenericItem *list, int index) {
    return private_get_item(list,index)->value;
}

bool gl_insert(GenericItem *list, void *item, int index) {
    if(index == 0) {
        list->value = item;
        return true;
    }else {
        if(index >= gl_list_size(list)) return false;
        GenericItem* before = private_get_item(list, index-1);
        GenericItem* after = before->next_item;
        GenericItem* itm = private_create_item(item);
        before->next_item = itm;
        itm->next_item = after;
        return true;
    }

}

int gl_list_size(GenericItem *list) {
    int count = 0;
    GenericItem* item = list;
    while(item->next_item) {
        item = item->next_item;
        count++;
    }

    return count;
}

void gl_delete_list(GenericItem* list) {
    for(int i = gl_list_size(list); i >= 0; i++) {
        GenericItem* item = private_get_item(list,i);
        free(item);
    }
}

GenericItem* private_get_item(GenericItem* list, int index) {
    if(gl_list_size(list) >= index) return NULL;

    GenericItem* item = list;
    for(int i = 1; i < index; i++) {
        item = item->next_item;
    }
    return item;
}

GenericItem* private_create_item(void* value) {
    GenericItem* itm = malloc(sizeof(GenericItem));
    itm->value = value;
    return itm;
}