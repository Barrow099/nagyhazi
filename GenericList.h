//
// Created by barrow099 on 10/25/18.
//

#ifndef NAGYHAZI_GENERICLIST_H
#define NAGYHAZI_GENERICLIST_H

#include <stdbool.h>

typedef struct GenericItem {
    void* value;
    struct GenericItem* next_item;
} GenericItem;

GenericItem* gl_create_list();
bool gl_insert(GenericItem* list, void* item,int index);
void* gl_get_item(GenericItem* list, int index);
int gl_list_size(GenericItem* list);
void gl_delete_list();

#endif //NAGYHAZI_GENERICLIST_H
