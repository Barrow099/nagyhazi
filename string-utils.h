//
// Created by barrow099 on 10/24/18.
//

#ifndef NAGYHAZI_STRING_UTILS_H
#define NAGYHAZI_STRING_UTILS_H

#include "types.h"

int stra_max(char *strs[], int num);
string* allocate_string_array(int items, int item_length);
void free_string_array(string*, int);
void strrmv(string, int);

#endif //NAGYHAZI_STRING_UTILS_H
