//
// Created by barrow099 on 10/9/18.
//

#ifndef NAGYHAZI_STRINGS_H
#define NAGYHAZI_STRINGS_H

#define STR_GOODBYE "Goodbye."

#define STR_MENU_YOURCHOICE "Your choice: "
#define STR_MENU_EXIT "Exit"
#define STR_MENU_INVALID "Invalid option!"
#define STR_MENU_BACK_TO_MAIN "Back to main menu"

#define STR_MENU_ADMIN_FLIGHTS "Flight data"
#define STR_MENU_ADMIN_AIRPORTS "Airport data"
#define STR_MENU_ADMIN_QUESTION "Please select task"

#define STR_MENU_FLIGHTS_LIST "List all flights"
#define STR_MENU_FLIGHTS_ADD "Create new flight"


#define STR_MENU_MAIN_QUESTION "Please select from the following modes"
#define STR_MENU_MAIN_USER "User mode: Book flights and select from the menu"
#define STR_MENU_MAIN_ADMIN "Admin mode: Create, modify and delete flights, edit flight reservation data"


#endif //NAGYHAZI_STRINGS_H
